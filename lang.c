#include "lang.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "mlist.h"
#include "mstring.h"
#include "merror.h"

#define CHECK_VAR_CHAR(c) ((c >= 97 && c <= 122) || (c >= 65 && c <= 90) || (c >= 48 && c <= 57) || c == 95)

mlist* vars;
mlist* funcs;
mvalue funcretr;
mstate* globalstate = NULL;
mstate* currentstate;

static void str_clear(char* s){
    for (int i = 0; s[i] != '\0'; i++)
    s[i] = '\0';
}

static void str_tolower(char* s){
    for (int i = 0; s[i] != '\0'; i++){
        if (s[i] >= 65 && s[i] <= 90)
            s[i] += 32;
    }
}

static mvalue get_default_var_value(mtype type){
    mvalue ret;
    switch(type){
        case MINTEGER:
            ret.integer = 0;
            break;
        case MREAL:
            ret.real = 0;
            break;
        case MBOOLEAN:
            ret.boolean = MFALSE;
            break;
        case MLIST:
            ret.list = list_create();
            break;
        case MSTRING:
            ret.string = string_new("");
            break;
        case MCHARACTER:
            ret.character = '\0';
            break;
        default:
            break;
    }
    return ret;
}

mstate* state_create(mstate* parent){
    mstate* ret = (mstate*)malloc(sizeof(mstate));
    if (ret == NULL) goto error;
    if (ret->parent != NULL)
        ret->parent = parent;
    else {
        if (globalstate != NULL)
            ret->parent = globalstate;
        else {
            globalstate = ret;
            ret->parent = NULL;
        }
    }
    ret->funcs  = list_create();
    ret->vars   = list_create();
    if (ret->funcs == NULL || ret->vars == NULL) goto error;
    return ret;

error:
    fprintf(stderr, "Error: Not enough memory.\n");
    exit(1);
}

void state_destroy(mstate* state){
    list_destroy(state->funcs);
    list_destroy(state->vars);
    if (state == currentstate){
        if (state->parent != NULL)
            currentstate = state->parent;
        else
            currentstate = globalstate;
    }
    free(state);
}

mtype str_to_type(char* type){
    str_tolower(type);
    if (!strcmp(type, MTYPE_INT)){
        return MINTEGER;
    }
    else if (!strcmp(type, MTYPE_FLOAT)){
        return MREAL;
    }
    else if (!strcmp(type, MTYPE_FLOAT)){
        return MSTRING;
    }
    else if (!strcmp(type, MTYPE_BOOLEAN)){
        return MBOOLEAN;
    }
    else if (!strcmp(type, MTYPE_CHAR)){
        return MCHARACTER;
    }
    else if (!strcmp(type, MTYPE_STRING)){
        return MSTRING;
    }
    else if (!strcmp(type, MTYPE_LIST)){
        return MLIST;
    }
    return -1;
}

static char* type_to_str(mtype type){
    char* ret;
    switch(type){
        case MCHARACTER:
            ret = (char*)malloc(sizeof(char)*(strlen(MTYPE_CHAR) + 1));
            strcpy(ret, MTYPE_CHAR);
            return ret;
        case MSTRING:
            ret = (char*)malloc(sizeof(char)*(strlen(MTYPE_STRING) + 1));
            strcpy(ret, MTYPE_STRING);
            return ret;
        case MINTEGER:
            ret = (char*)malloc(sizeof(char)*(strlen(MTYPE_INT) + 1));
            strcpy(ret, MTYPE_INT);
            return ret;
        case MREAL:
            ret = (char*)malloc(sizeof(char)*(strlen(MTYPE_FLOAT) + 1));
            strcpy(ret, MTYPE_FLOAT);
            return ret;
        case MBOOLEAN:
            ret = (char*)malloc(sizeof(char)*(strlen(MTYPE_BOOLEAN) + 1));
            strcpy(ret, MTYPE_BOOLEAN);
            return ret;
        case MVOID:
            ret = (char*)malloc(sizeof(char)*(strlen(MTYPE_VOID) + 1));
            strcpy(ret, MTYPE_VOID);
            return ret;
        case MFUNCTION:
            ret = (char*)malloc(sizeof(char)*(strlen(MTYPE_FUNCTION) + 1));
            strcpy(ret, MTYPE_FUNCTION);
            return ret;
        case MLIST:
            ret = (char*)malloc(sizeof(char)*(strlen(MTYPE_LIST) + 1));
            strcpy(ret, MTYPE_LIST);
            return ret;
    }
}

static void var_set_to_var(mvar* var1, const mvar* var2){
    if (var1->type == var2->type){
        var1->value = var2->value;
    }
    else if (var1->type == MINTEGER && var2->type == MREAL){
        var1->value.integer = var2->value.real;
    }
    else if (var2->type == MINTEGER && var1->type == MREAL){
        var1->value.real = var2->value.integer;
    }
    else {
        fprintf(stderr, "Error: tried to assign value of %s, which is of type %s, to variable %s of type %s.\n", 
                var2->name,
                type_to_str(var2->type),
                var1->name,
                type_to_str(var2->type));
        return;
    }
}

void func_create(mstate* state,
                 char*  name,
				 mtype  rtype,
				 mtype* argt,
				 int    argc,
				 mproc* proc,
				 int    procc){
	mfunc* func = (mfunc*)malloc(sizeof(mfunc));
	if (func == NULL) goto error;
    if (!CHECK_VAR_CHAR(name[0])){
        fprintf(stderr, "Error: tried to create function with invalid name.\n");
        return;
    }
    func->name  = (char*)malloc(sizeof(char)*(strlen(name)+1));
    strncpy(func->name, name, MAXIMUM_FUNC_NAME_LENGTH);
	func->rtype = rtype;
	func->argc  = argc;
	func->procc = procc;
	func->argt  = (mtype*)malloc(sizeof(mtype)*argc);
	func->procs = (mproc*)malloc(sizeof(mproc)*procc);
	if (func->argt == NULL || func->procs == NULL) goto error;
	for (int i = 0; i < argc; i++){
		func->argt[i] = argt[i];
	}
	for (int i = 0; i < procc; i++){
		func->procs[i] = proc[i];
	}
	list_add(state->funcs, func);

error:
	fprintf(stderr, "Error: Not enough memory.\n");
	exit(1);
}

void func_exec(mstate* state, mfunc* func){
	for (int i = 0; i < func->procc; i++){
		(func->procs[i])(state);
	}
}	 

mvar* var_find(mstate* state, char* varname){
    if (state->vars == NULL) return NULL;
    list_slot_t* el = state->vars->start;
    list_iterate(state->vars){
        mvar* var = (mvar*)el->element;
        if (!strcmp(var->name, varname)) return var;
        el = el->next;
    }
    if (state->parent != NULL) return var_find(state->parent, varname);
    return NULL;
}

void var_create(mstate* state, char* name, mtype type, mvalue value){
    mvar* var;
    if (!CHECK_VAR_CHAR(name[0])){
        fprintf(stderr, "Error: tried to create variable with invalid name.\n");
        return;
    }
    if (var_find(currentstate, name) != NULL){
        fprintf(stderr, "Error: redeclaration of existing variable '%s'\n", name);
        return;
    }
    var = (mvar*)malloc(sizeof(mvar));
    if (var == NULL) goto error;
    var->name  = (char*)malloc(sizeof(char)*(strlen(name)+1));
    strcpy(var->name, name);
    var->type  = type;
    var->value = value;
    list_add(state->vars, var);
    return;

error:
    fprintf(stderr, "Error: Not enough memory.\n");
    exit(1);
}

static void eval_str(mstate* state, char* s, mvalue* retval, mtype* rettype){
    //Skip whitespace:
    if (s[0] == ' '){
        int i;
        for (i = 0; s[i] == ' '; i++);
        eval_str(state, &s[i], retval, rettype);
        return;
    }

    //Boolean:
    if (!strcmp(s, BOOLFALSE)){
        retval->boolean = MFALSE;
        *rettype = MBOOLEAN;
    }
    else if (!strcmp(s, BOOLTRUE)){
        retval->boolean = MTRUE;
        *rettype = MBOOLEAN;
    }

    //Number:
    else if ((s[0] >= '0' && s[0] <= '9') || s[0] == '.'){
        int decim;
        *rettype = MINTEGER;   
        retval->integer = 0;
        for (int i = 0; s[i] != '\0'; i++){
            if (!((s[i] >= '0' && s[i] <= '9') || s[i] == '.')){
                fprintf(stderr, "Error: invalid value '%s' for number type.\n", s);
                return;
            }
            if (s[i] == '.' && *rettype == MINTEGER){
                *rettype = MREAL;
                retval->real = retval->integer;
                decim = 1;
            }
            switch(*rettype){
                case MINTEGER:
                    retval->integer *= 10;
                    retval->integer += s[i] - '0';
                    break;
                case MREAL:
                    retval->real += (s[i] - '0')/decim;
                    break;
            }
        }
    }

    //String or character:
    else if (s[0] == '\"' || s[0] == '\''){
        int i;
        for (i = 1; s[i] != s[0]; i++){
            if (s[i] == '\0' || s[i] == EOF){
                fprintf(stderr, "Error: unfinished string.\n");
                return;
            }
        }
        if (i == 2){
            retval->character = '\0';
            *rettype = MCHARACTER;
        }
        else if (i == 2){
            retval->character = s[1];
            *rettype = MCHARACTER;
        }
        else if (i > 2){
            if (rettype == MSTRING) string_delete(retval->string);
            retval->string = string_new((char*)malloc(sizeof(char)*i));
            if (retval->string == NULL){
                fprintf(stderr, "Error: Out of memory.\n");
                exit(1);
            }
            strncpy(retval->string->str, &s[1], i-1);
            retval->string->str[i] = '\0';
            *rettype = MSTRING;
        }
        i++;
        while (s[i] == ' ') i++;
        if (s[i] == '+'){            
            mvalue temp;
            eval_str(state, &s[i], retval, rettype);
            return;
        }
        else if (s[i] != '\0' && s[i] != EOF){
            fprintf(stderr, "Error: Syntax error.\n");
            return;
        }
    }

    //Variable:
    else {
        mvar* var = var_find(state, s);
        if (var != NULL){
            *retval = var->value;
            *rettype = var->type;
        }
        else {
            fprintf(stderr, "Error: Var '%s' not found.");
        }
    }
}

void readcmd(char* cmd){
    int i, j = 0;
    mvar* lvalue;
    mvalue rvalue;
    mtype type;
	char *varname, 
		 *funcname,
		 *temp,
		 *recursive_buffer;	
    if (cmd[strlen(cmd)-1] == '\n') cmd[strlen(cmd) - 1] = '\0';
    //Detect command:
    temp = (char*)alloca(sizeof(char) * (1 + strlen(cmd)));
    if (temp == NULL){
        fprintf(stderr, "Error: Out of memory.\n");
        goto leavecmd;
    }
    for (i = 0; cmd[i] != '\0'; i++){
        temp[i] = cmd[i];
        temp[i+1] = '\0';

        if (i == 3){
            if (!strcmp(temp, SETVAR1)){
                i++;
                goto setvar;
            }
            else if (!strcmp(temp, NEWVAR1)){
                i++;
                goto newvar;
            }
        }
		else if (i == 8){
			if (!strcmp(temp, SETFUNC)){
				i++;
                goto newfunc;
			}
		}
    }
    printf("Unknown command '%s'.\n", cmd);
    goto leavecmd;

setvar:
    while (cmd[i] == ' '){
        i++;
    }
    str_clear(temp);
    while (cmd[i] != ' '){
        temp[j] = cmd[i];
        if (cmd[i] == '\0'){ 
            fprintf(stderr, "Error: Syntax error.\n");
            goto leavecmd;
        }
        if (cmd[i] == '\0') break;
        j++; i++;
        temp[j] = '\0';
    }
    lvalue = var_find(currentstate, temp);
    if (lvalue == NULL){
		printf( "Error: Unknown variable '%s', declare it with 'new <type> %s: <value>' before use.\n", temp, temp);
    	goto leavecmd;
	}
	type = lvalue->type;
    j = 0;
    while (cmd[i] == ' '){
        i++;
    }
    str_clear(temp);
    while (cmd[i] != ' '){
        temp[j] = cmd[i];
        j++; i++;
        temp[j] = ' ';
    }
    if (strcmp(temp, SETVAR2)){
		fprintf(stderr, "Error: Syntax error, 'to' expected.\n");
		goto leavecmd;
	}
    while (cmd[i] == ' '){
        i++;
    }
    str_clear(temp);
    j = 0;
    while (cmd[i] != '\0' && cmd[i] != '\n' && cmd[i] != ' '){
        temp[j] = cmd[i];
        j++; i++;
	temp[j] = '\0';
    }
    list_slot_t* el;
    char entrychar;
    mtype rtype;
    eval_str(currentstate, temp, &rvalue, &rtype);
    if (rtype == lvalue->type){
        lvalue->value = rvalue;
        goto leavecmd;
    }
    else {
        char *buffer1 = type_to_str(rtype),
             *buffer2 = type_to_str(lvalue->type);
        fprintf(stderr, "Error: Trying to assign %s value to %s variable '%s'.", buffer1, buffer2, lvalue->name);
        free(buffer1); free(buffer2);
        goto leavecmd;
    }
    
    goto leavecmd;

newfunc:
    while (cmd[i] == ' '){
        i++;
    }
	str_clear(temp);
	while (cmd[i] != '('){
        if (cmd[i] != ' ') temp[j] = cmd[i];
		if (cmd[i] == '\0') {
			fprintf(stderr, "Error: Syntax error\n");
			goto leavecmd;
		}
		if (cmd[i] == '\0') break;
		if (cmd[i] == ' ') continue;
		j++; i++;
		temp[j] = '\0';
	}
	funcname = (char*)alloca(sizeof(char)*(strlen(temp)+1));
	strcpy(funcname, temp);

    mtype* argt = (mtype*)malloc(0);
    int funcargc = 0;
	j = 0;
    while (cmd[i] == ' '){
        i++;
    }
    if (cmd[i] != '('){
        fprintf(stderr, "Error: Syntax error.\n");
        goto leavecmd;
    }
	while (cmd[i] != ')'){
        if (cmd[i] == '\0'){
            fprintf(stderr, "Error: Syntax error.\n");
            goto leavecmd;
        }
        while (cmd[i] == ' '){
            i++;
        }
        if (temp[0] != '\0'){
            funcargc++;
            argt = (mtype*)realloc(argt, funcargc);
            argt[funcargc - 1] = str_to_type(temp);
            if (argt[funcargc - 1] == -1){
                fprintf(stderr, "Error: Invalid type.\n");
                goto leavecmd;
            }
        }
		str_clear(temp);
		while (cmd[i] != ' '){
			temp[j] = cmd[i];
            temp[j+1] = '\0';
		}
	}
	goto leavecmd;

newvar:
    while (cmd[i] == ' '){
        i++;
    }
    str_clear(temp);
    while (cmd[i] != ' '){
        temp[j] = cmd[i];
        if (cmd[i] == '\0'){
			fprintf(stderr, "Error: Syntax error\n");
			goto leavecmd;
		}
        if (cmd[i] == '\0') break;
        j++; i++;
        temp[j] = '\0';
    }
    mtype vartype = str_to_type(temp);
    if (vartype == -1){
		fprintf(stderr, "Error: Unknown type '%s'\n", temp);
		goto leavecmd;
	}
    while (cmd[i] == ' '){
        i++;
    }
    str_clear(temp);
    j = 0;
    while (cmd[i] != ':' && cmd[i] != '\0'){
        if (!(CHECK_VAR_CHAR(cmd[i]) && !(cmd[i] >= 48 && cmd[i] <= 57 && j == 0))){
			printf( "Error: Invalid '%s%c' variable declaration. A variable cannot start with numbers, and can only be written with letters, numbers or underscores.\n", temp, cmd[i]);
			goto leavecmd;
		}
        temp[j] = cmd[i];
        if (cmd[i] == '\0') {
			i++; 
			break;
		}
        j++; i++;
        temp[j] = '\0';
    }
    if (strlen(temp) >= MAXIMUM_VAR_NAME_LENGTH){
        fprintf(stderr, "Error: '%s' exceeds the amount of characters allowed for a variable. Maximum of %d characters.\n", temp, MAXIMUM_VAR_NAME_LENGTH);
        goto leavecmd;
    }
    varname = (char*)alloca(sizeof(char)*(strlen(temp)+1));
    strncpy(varname, temp, MAXIMUM_VAR_NAME_LENGTH);
    mvalue varvalue = get_default_var_value(vartype);
    if (cmd[i] != ':'){
        var_create(currentstate, temp, vartype, varvalue);
        goto leavecmd;
    }
    i++; j = 0;
    str_clear(temp);
    while (cmd[i] == ' '){
        i++;
        if (cmd[i]=='\0'){
			fprintf(stderr, "Error: Expected a variable value after ':', got '%s'.\nNote: If you only want to declare a variable, without giving it a value, it is possible to simply call 'new <type> <varname>'.\n", temp);
			goto leavecmd;
		}
    }
    while (cmd[i] != '\0'){
        temp[j] = cmd[i];
        i++; j++;
	    temp[j] = '\0';
    }
    recursive_buffer = (char*)alloca(sizeof(char)*(strlen(temp) + strlen(varname) + 10));
    strcpy(recursive_buffer, SETVAR1);
    strcat(recursive_buffer, varname);
    strcat(recursive_buffer, " ");
    strcat(recursive_buffer, SETVAR2);
    strcat(recursive_buffer, temp);
    var_create(currentstate, varname, vartype, varvalue);
    readcmd(recursive_buffer);
    
leavecmd:
    return;
}

