#include "merror.h"
#include <stdio.h>
#include <stdlib.h>

static void _ERRTHROW(merror error){
    if (error.errorscale == FATAL)
        exit(error.errcode);
}
