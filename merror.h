#ifndef MERROR_H
#define MERROR_H

#define ASSERT(condition, ...) if(!(condition)) {printf("\n"); printf(__VA_ARGS__); printf("\n");}

typedef enum {
    ERR_OutOfMemory,
    ERR_UnknownVar
} errtype;

typedef enum {
    MINOR,
    FATAL
} errscale;

typedef struct{
    errtype errortype;
    errscale errorscale;
    unsigned int errcode;
} merror;

#endif /* MERROR_H */
